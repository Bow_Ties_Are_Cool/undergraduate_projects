# Undergraduate Projects Links  

Here are all the projects for my undergraduate degree at Texas State University.  

# Class Projects  
These are all the projects required by the given class. 
  
All of these projects are private as requested by most professor to prevent  
cheating. Most of these have been added but I am going through and cleaning up  
minnor issues (not changing the functionality) and automating build process  
using cmake.  
  
Email me for access to these repos.  

### Private  

[CS 2408: Foundations of Computer Science II](https://gitlab.com/Bow_Ties_Are_Cool/CS_2408_Foundations_of_Computer_Science_II)  
[CS 3339: Computer Architecture](https://gitlab.com/Bow_Ties_Are_Cool/CS_3339_Computer_Architecture)  
[CS 3354: Object-Oriented Design and Programming] - Coming Soon  
[CS 4310: Computer Networks] - Coming Soon  
[CS 4328: Operating Systems] - Coming Soon  
[CS 4380: Parallel Programming] - Coming Soon  

# Personal Projects  
Here are some projects that I worked on outside of the scope of any of the above  
classes I have placed descripitions after the links.  
  
For private repos you can email me for access.  

### Private

[My Old Website](https://gitlab.com/Bow_Ties_Are_Cool/Old_Website): Here is the older version of my website that I built from scratch  
[My Current Website](https://gitlab.com/Bow_Ties_Are_Cool/Professional_Website): Here is my current website using a template from Colorlib  

### Public

[Pigeon Main](https://gitlab.com/Texas_State_Pigeon/Pigeon_Main_Project): This a bash file that allows users to send messages using Linux without needing root access **(Work In Progress)**  
[Pigeon Download](https://gitlab.com/Texas_State_Pigeon/Pigeon_Download): This is an install script for Pigeon writen in bash **(Work In Progress)**  
[Obsidian] - Coming Soon: This is a verilog project to design a chip based on ARM ISA  
[Linux Stats Automation] - Coming Soon: This was a project to understand Linux server usage at Texas State  
